﻿using Org.BouncyCastle.Asn1.Crmf;
using Smartsheet.Api;
using Smartsheet.Api.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartPDF
{
    public partial class frmLogin : Form
    {

        SmartsheetClient smartsheet;

        public frmLogin()
        {
            InitializeComponent();
        }

        public void SetConfig(Config config)
        {
            txtToken.Text = config.AccessToken;
            if (txtToken.Text.Length > 0)
            {
                CheckLogin();
            }
        }

        private void CheckLogin()
        {
            UserProfile userProfile;
            try
            {
                smartsheet = new SmartsheetBuilder().SetAccessToken(txtToken.Text).Build();

                userProfile = smartsheet.UserResources.GetCurrentUser();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Access Token invalid");
                lblCurrentUser.Text = "<none>";
                return;
            }

            lblCurrentUser.Text = userProfile.FirstName + " " + userProfile.LastName;
        }

        internal string GetToken()
        {
            return txtToken.Text;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            SearchResult results = smartsheet.SearchResources.Search(txtSheetName.Text, null, null, null, new List<SearchScope> { SearchScope.SHEET_NAMES });
            lvSheets.Items.Clear();
            foreach(var r in results.Results)
            {
                var lvi = new ListViewItem
                {
                    Tag = r.ObjectId,
                    Text = r.Text
                };
                lvSheets.Items.Add(lvi);
            }
        }

        private void btnCheckToken_Click(object sender, EventArgs e)
        {
            CheckLogin();
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        internal string GetSheetID()
        {
            if (lvSheets.SelectedItems.Count > 0)
            {
                return lvSheets.SelectedItems[0].Tag.ToString();
            }
            return "";
        }

        private void lvSheets_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnDone.Enabled = lvSheets.SelectedItems.Count > 0;
        }

        internal string GetSheetName()
        {
            if (lvSheets.SelectedItems.Count > 0)
            {
                return lvSheets.SelectedItems[0].Text;
            }
            return "";
        }

        internal string GetUserName()
        {
            return lblCurrentUser.Text;
        }
    }
}
