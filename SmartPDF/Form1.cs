﻿using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using Smartsheet.Api;
using Smartsheet.Api.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartPDF
{
    public partial class Form1 : Form
    {
        private readonly Config Config;
        private SmartsheetClient smartsheet;
        private Dictionary<string, string> rowData;

        public Form1()
        {
            InitializeComponent();
            Config = Config.GetConfig();
        }

        private void GetSmartDataByRow()
        {
            if (!int.TryParse(txtRow.Text, out _))
            {
                MessageBox.Show("Invalid Row Number");
                return;
            }
            smartsheet = new SmartsheetBuilder().SetAccessToken(Config.AccessToken).Build();
            var result = new Dictionary<string, string>();

            Sheet sheet = smartsheet.SheetResources.GetSheet(
                long.Parse(Config.SheetId),           // sheetId
                null,                       // IEnumerable<SheetLevelInclusion> includes
                null,                       // IEnumerable<SheetLevelExclusion> excludes
                null,                       // IEnumerable<long> rowIds
                new List<int> { int.Parse(txtRow.Text) },                       // IEnumerable<int> rowNumbers
                null,                       // IEnumerable<long> columnIds
                null,                       // Nullable<long> pageSize
                null                        // Nullable<long> page
            );

            lvData.Items.Clear();
            rowData = new Dictionary<string, string>();

            var r = sheet.Rows.First();
            lvData.Columns.Add("Row Number");
            lvData.Items.Add(r.RowNumber.ToString());
            foreach (var c in r.Cells)
            {
                var lviValue = new ListViewItem.ListViewSubItem
                {
                    Text = c.DisplayValue
                };

                string title = sheet.GetColumnById(c.ColumnId.Value).Title;
                lvData.Columns.Add(title);
                lvData.Items[0].SubItems.Add(lviValue);

                rowData.Add(title, c.DisplayValue);

            }

            // update generated file name preview
            lblOutput.Text = GetOutputFilename();
        }

        private string GetOutputFilename()
        {
            if (rowData == null) return "<no preview available>";
            Dictionary<string, Func<string>> replacements = new Dictionary<string, Func<string>>();
            replacements.Add("Source File Name", () => { return System.IO.Path.GetFileNameWithoutExtension(Config.LastPdfPath); });
            // Row Number is synthetically added to RowData, so no need to handle explicitly
            replacements.Add("Sheet Name", () => { return GetSheetNameById(Config.SheetId); });
            replacements.Add("Sheet User Name", () => { return GetUserName(); });

            string pattern = txtPattern.Text;

            foreach (var kvp in rowData)
            {
                string match = "[" + kvp.Key + "]";
                if (pattern.Contains(match))
                {
                    pattern = pattern.Replace(match, kvp.Value);
                }
            }

            foreach (var kvp in replacements)
            {
                string match = "[" + kvp.Key + "]";
                if (pattern.Contains(match))
                {
                    pattern = pattern.Replace(match, kvp.Value.Invoke());
                }
            }

            return pattern;
        }

        private string GetSheetNameById(string id)
        {
            if (smartsheet == null)
            {
                smartsheet = new SmartsheetBuilder().SetAccessToken(Config.AccessToken).Build();
            }

            Sheet sheet = smartsheet.SheetResources.GetSheet(
                long.Parse(Config.SheetId),           // sheetId
                null,                       // IEnumerable<SheetLevelInclusion> includes
                null,                       // IEnumerable<SheetLevelExclusion> excludes
                null,                       // IEnumerable<long> rowIds
                new List<int> { 1 },                       // IEnumerable<int> rowNumbers
                null,                       // IEnumerable<long> columnIds
                null,                       // Nullable<long> pageSize
                null                        // Nullable<long> page
            );

            return sheet.Name;
        }

        /// <summary>  
        /// List all of the form fields into a textbox.  The  
        /// form fields identified can be used to map each of the  
        /// fields in a PDF.  
        /// </summary>  
        private List<string> ListFieldNames()
        {
            List<string> result = new List<string>();
            // create a new PDF reader based on the PDF template document  
            PdfReader pdfReader = new PdfReader(Config.LastPdfPath);
            // add all fields to the result list
            foreach (var de in pdfReader.AcroFields.Fields)
            {
                result.Add(de.Key.ToString());
            }
            return result;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Config.SaveConfig(Config);
            Application.Exit();
        }

        private void btnSource_Click(object sender, EventArgs e)
        {
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                Config.LastPdfPath = ofd.FileName;
                lblFilename.Text = System.IO.Path.GetFileName(Config.LastPdfPath);
            }
            else
            {
                lblFilename.Text = "<none>";
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (Config.SheetId?.Length > 0 && Config.AccessToken?.Length > 0)
            {
                gbGenerate.Enabled = true;
                // selected sheet name display
                lblSheetName.Text = GetSheetNameById(Config.SheetId);
            }
            if (Config.LastPdfPath?.Length > 0)
            {
                lblFilename.Text = System.IO.Path.GetFileName(Config.LastPdfPath);
            }
            else
            {
                lblFilename.Text = "<none>";
            }

            if (Config.LastOutputPattern?.Length > 0)
            {
                txtPattern.Text = Config.LastOutputPattern;
            }

        }

        private void btnAuth_Click(object sender, EventArgs e)
        {
            var lf = new frmLogin();
            lf.SetConfig(Config);
            lf.ShowDialog();
            Config.AccessToken = lf.GetToken();
            Config.SheetId = lf.GetSheetID();

            if (Config.SheetId.Length > 0 && Config.AccessToken.Length > 0)
            {
                gbGenerate.Enabled = true;
            }
        }

        private string GetUserName()
        {
            UserProfile userProfile;
            try
            {
                if (smartsheet == null)
                {
                    smartsheet = new SmartsheetBuilder().SetAccessToken(Config.AccessToken).Build();
                }
                userProfile = smartsheet.UserResources.GetCurrentUser();
            }
            catch (Exception ex)
            {
                return "";
            }
            return userProfile.FirstName + " " + userProfile.LastName;
        }

        private void btnChooseRow_Click(object sender, EventArgs e)
        {
            GetSmartDataByRow();
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {

            // write to log init info
            txtLog.Clear();
            txtLog.AppendText("Generating PDF from row " + txtRow.Text + Environment.NewLine);

            // write known column names to log
            txtLog.AppendText("SmartSheet column titles are: " + string.Join(", ", rowData.Keys) + Environment.NewLine);
            txtLog.AppendText("SmartSheet row values are: " + string.Join(", ", rowData.Values) + Environment.NewLine);

            // get list of fields PDF has
            List<string> pdfFields = ListFieldNames();

            // write PDF field names to log
            txtLog.AppendText("PDF fields are: " + string.Join(", ", pdfFields) + Environment.NewLine);

            // suggest the filename based on the pattern
            sfd.FileName = GetOutputFilename();

            txtLog.AppendText("Pattern is: " + txtPattern.Text + Environment.NewLine);
            txtLog.AppendText("Generated file name is: " + sfd.FileName + ".pdf" + Environment.NewLine);

            if (Config.LastOutputPath?.Length > 0)
            {
                sfd.InitialDirectory = Config.LastOutputPath;
            }

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                Config.LastOutputPath = System.IO.Path.GetDirectoryName(sfd.FileName);
                txtLog.AppendText("PDF target path is : " + sfd.FileName + Environment.NewLine);

                // write values in source PDF (memory)
                string pdfTemplate = Config.LastPdfPath;
                string newFile = sfd.FileName;
                PdfReader pdfReader = new PdfReader(pdfTemplate);
                PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(newFile, FileMode.Create));
                AcroFields pdfFormFields = pdfStamper.AcroFields;
                // set form pdfFormFields  
                foreach (var f in pdfFields)
                {
                    if (rowData.ContainsKey(f))
                    {
                        pdfFormFields.SetField(f, rowData[f]);
                    }
                }

                // flatten the form to remove editting options, set it to false  
                // to leave the form open to subsequent manual edits  
                pdfStamper.FormFlattening = false;
                // close the pdf  
                pdfStamper.Close();

                Process.Start(newFile);
            } else
            {
                txtLog.AppendText("User canceled the operation" + Environment.NewLine);
            }
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            var hlp = new frmHelp();
            hlp.Show();
        }

        private void txtPattern_TextChanged(object sender, EventArgs e)
        {
            // update generated file name preview
            lblOutput.Text = GetOutputFilename();
            Config.LastOutputPattern = txtPattern.Text;
        }

        private void btnPatternHelp_Click(object sender, EventArgs e)
        {
            var hlp = new frmPatternHelp();
            hlp.Show();
        }
    }
}
